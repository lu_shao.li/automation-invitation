﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Microsoft.Office.Interop.Word;

using System.Threading;
using System.Data.OleDb;
using System.Data;

namespace WindowsFormsApplication1
{
    public partial class CCNFF : Form
    {
        public CCNFF()
        {
            InitializeComponent();
            this.Text = "文件產生器";
        }



        public void SearchReplace(string findText, string replaceText, string source, string sink) 
        {
            

            Microsoft.Office.Interop.Word.Application appWord = new Microsoft.Office.Interop.Word.Application();

            Microsoft.Office.Interop.Word.Document doc = new Document();

            object oMissing = System.Reflection.Missing.Value;

            //打開模板文檔，並指定doc的文檔類型

            //object path = @"C:\Users\紹立\Downloads\2015.docx";      
            object objSource = source;
            object objDocType = WdDocumentType.wdTypeDocument;
            object objfalse = false;
            object objtrue = true;
            doc = (Document)appWord.Documents.Add(ref objSource, ref objfalse, ref objDocType, ref objtrue);



           // object obDD_Name = "CCNF";
            object objfindText = findText;

            appWord.Selection.Find.ClearFormatting();
            appWord.Selection.Find.Text = findText.ToString();

            appWord.Selection.Find.Replacement.ClearFormatting();
            appWord.Selection.Find.Replacement.Text = replaceText;
            object replaceAll = Microsoft.Office.Interop.Word.WdReplace.wdReplaceAll;
            if (appWord.Selection.Find.Execute(ref objfindText,
    ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
    ref oMissing, ref oMissing, ref oMissing, ref replaceAll, ref oMissing, ref oMissing,
    ref oMissing, ref oMissing))
            {

                //MessageBox.Show("Text found.");
            }
            else
            {
                //MessageBox.Show("The text could not be located.");
            }





            //第四步 生成word，將當前的文檔對象另存爲指定的路徑，然後關閉doc對象。關閉應用程序
            object filename = sink;
           // object filename = @"C:\Users\紹立\Downloads\CCNF" + ".docx";//HttpContext.Current.Server.MapPath("f:\\") + "Testing_" + DateTime.Now.ToShortDateString() + ".doc";

            object miss = System.Reflection.Missing.Value;
            object fileFormat = WdSaveFormat.wdFormatPDF;
            doc.SaveAs(ref filename, ref fileFormat, ref miss, ref miss, ref miss, ref miss,

                ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss);

            object missingValue = Type.Missing;
            
            object doNotSaveChanges = WdSaveOptions.wdDoNotSaveChanges;

            doc.Close(ref doNotSaveChanges, ref missingValue, ref missingValue);

            appWord.Application.Quit(ref miss, ref miss, ref miss);

            doc = null;
            
            appWord = null;

           // MessageBox.Show("Successful！");

          //  System.Diagnostics.Process.Start(filename.ToString());//打開文檔
        }
        public void readExcel() 
        {
            string dataTimeStart = System.DateTime.Now.ToLongTimeString();
            //Button1.Enabled = false;
            //Button1.Text = "已傳送";

            /**************/
            /**************/
            /***目標程式***/
            /**************/
            /**************/

            //string rootpath = Request.PhysicalApplicationPath; //抓取專案所在實際目錄路徑
            //DirectoryInfo docspath = new DirectoryInfo(rootpath); // 搭配專案相對應上傳的路徑
           // DirectoryInfo docspath = new DirectoryInfo(@textBoxSave.Text); 
            //設定讀取的Excel屬性
            //設定讀取的Excel屬性
            string strCon = "Provider=Microsoft.Ace.OleDb.12.0;" +
            "Data Source=" + textBoxExcel.Text+";"  +
                //選擇Excel版本
                //Excel 12.0 針對Excel 2010、2007版本(OLEDB.12.0)
                //Excel 8.0 針對Excel 97-2003版本(OLEDB.4.0)
                //Excel 5.0 針對Excel 97(OLEDB.4.0)
            "Extended Properties='Excel 12.0;" +
            //開頭是否為資料
                //若指定值為 Yes，代表 Excel 檔中的工作表第一列是欄位名稱，oleDB直接從第二列讀取
                //若指定值為 No，代表 Excel 檔中的工作表第一列就是資料了，沒有欄位名稱，oleDB直接從第一列讀取
            "HDR=Yes;" +

            //IMEX=0 為「匯出模式」，能對檔案進行寫入的動作。
                //IMEX=1 為「匯入模式」，能對檔案進行讀取的動作。
                //IMEX=2 為「連結模式」，能對檔案進行讀取與寫入的動作。
            "IMEX=1'";
            /*步驟2：依照Excel的屬性及路徑開啟檔案*/

            //Excel路徑及相關資訊匯入
            OleDbConnection GetXLS = new OleDbConnection(strCon);

            //打開檔案
            GetXLS.Open();
            /*步驟3：搜尋此Excel的所有工作表，找到特定工作表進行讀檔，並將其資料存入List*/

            //搜尋xls的工作表(工作表名稱需要加$字串)
            System.Data.DataTable Table = GetXLS.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

            //查詢此Excel所有的工作表名稱
            string SelectSheetName = "";

            foreach (DataRow row in Table.Rows)
            {
                //抓取Xls各個Sheet的名稱(+'$')-有的名稱需要加名稱''，有的不用
                SelectSheetName = (string)row["TABLE_NAME"];

                //工作表名稱有特殊字元、空格，需加'工作表名稱$'，ex：'Sheet_A$'
                //工作表名稱沒有特殊字元、空格，需加工作表名稱$，ex：SheetA$
                //所有工作表名稱為Sheet1，讀取此工作表的內容
                if (SelectSheetName == "工作表1$")
                {
                    //select 工作表名稱
                    OleDbCommand cmSheet1 = new OleDbCommand(" SELECT * FROM [工作表1$] ", GetXLS);
                    OleDbDataReader drSheet1 = cmSheet1.ExecuteReader();

                    //讀取工作表SheetA資料
                    List<string> ListSheetA = new List<string>();
                  
                    while (drSheet1.Read())
                    {
                        try
                        {
                            textBoxDisplay.Text += drSheet1[0].ToString() + " " + drSheet1[1].ToString() ;
                            SearchReplace("XXX", drSheet1[1].ToString(), @textBoxSource.Text, @textBoxSave.Text+@"\" + drSheet1[0].ToString()+".pdf");
                        }
                        catch (Exception ex)
                        {
                           // MessageBox.Show("Excel Error！");
                            textBoxDisplay.ForeColor = System.Drawing.Color.Red;
                            textBoxDisplay.Text += " Fail!\r\n";
                            textBoxDisplay.Text += ex.ToString();
                            break;
                            textBoxDisplay.ForeColor = System.Drawing.Color.Black;
                        }
                        finally
                        {
                            textBoxDisplay.ForeColor = System.Drawing.Color.Green;
                            textBoxDisplay.Text += " Successful！\r\n";
                            textBoxDisplay.ForeColor = System.Drawing.Color.Black;
                            //textBoxDisplay.Text += "===================";
                        }



                    }


                    /*步驟4：關閉檔案*/

                    //結束關閉讀檔(必要，不關會有error)
                    drSheet1.Close();
                    GetXLS.Close();
                    
                    GetXLS.Dispose();
                    drSheet1.Dispose();

                 
                }

            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            
            label2.Text = "程式執行中...";
            readExcel();
        
            label2.Text = "已完成!";
         
        }
      
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();

            textBoxSource.Text = openFileDialog1.FileName;
            textBoxSave.Text = Path.GetDirectoryName(textBoxSource.Text);

           
           
           
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            openFileDialog2.ShowDialog();
            textBoxExcel.Text = openFileDialog2.FileName;
        }

      
    }
}
